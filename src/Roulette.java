import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

class Roulette {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		RouletteWheel rouletteWheel = new RouletteWheel();
		boolean quit = false;
		while (!quit) {
			System.out.println("To quit enter 'q'");
			System.out.print("Would you like to bet? y/n: ");
			String option = scanner.nextLine();

			char wantBet;
			if (option.length() != 1) {
				wrongInput();
				continue;
			} else {
				wantBet = option.charAt(0);
			}

			if (wantBet == 'y') {
				int betNumber = 0;
				boolean correctBetNumber = false;
				while (!correctBetNumber) {
					System.out.println("In which number do you want to bet?");
					try {
						betNumber = scanner.nextInt();
						if (betNumber < 0 || betNumber >= 37) {
							System.out.println("The number must be between 0 and 37");
							continue;
						}
						correctBetNumber = true;
					} catch (InputMismatchException e) {
						System.out.println("Only enter a number");
						scanner.nextLine();
					}
				}

				double betMoney = 0d;
				boolean correctBet = false;
				while (!correctBet) {
					System.out.println("How much would you like to bet?");
					try {
						betMoney = scanner.nextDouble();
						if (betMoney <= 0) {
							System.out.println("Betting amount must be greater than 0");
							continue;
						}
						correctBet = true;
					} catch (InputMismatchException e) {
						System.out.println("Enter only a number");
						scanner.nextLine();
					}
				}
				rouletteWheel.spin();
				int winningNumber = rouletteWheel.getValue();
				System.out.println("The winning number is " + winningNumber);

				if (winningNumber == betNumber) {
					System.out.println("Congratulations you won " + betMoney * 35 + betMoney);
				} else {
					System.out.println("You lost " + betMoney);
				}

			} else if (wantBet == 'n') {
				System.out.println("See you next round");
			} else if (wantBet == 'q') {
				System.out.println("Goodbye");
				quit = true;
			} else {
				wrongInput();
			}
		}
	}

	private static void wrongInput() {
		System.out.println("Please enter only q,y,n");
	}
}